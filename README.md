# Container Basics

Este repositório contém exemplos e tutoriais básicos sobre o uso de contêineres, incluindo Docker e CI/CD.

### Build
```
docker build -t registry.gitlab.com/nataliagranato/container-basics .
```
### Push
```
docker push registry.gitlab.com/nataliagranato/container-basics
```
### Executando a imagem do contêiner
```
docker container run -d -p 8000:90 container-basics:1d44b0ee
```

### Acessando o app
Se seu comando foi executado com sucesso, agora você pode acessar seu contêiner em http://localhost:8000

### Parando o container
Para parar o contêiner, primeiro você precisa descobrir o ID do contêiner.

Abra outra sessão do seu terminal para que você possa acompanhar os comandos abaixo.

Primeiro, execute o comando docker ps.

```
docker container stop ID
```
